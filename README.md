# PinTrac MDM Evergreen Automation
Automate disabling overdue hotspots, and re-enabling returned hotspots using pintrac MDM from Franklin with the Evergreen Library Automation system.

Update: December 2023 - Pintrac is aware of this and is fine with customers using this.  I'm not sure how much they will support it.  But you can always check in with me if you have problems.

Pintrac is a subscription based service, with a very reasonable fee for a year of service.  It works with Franklin hotspots only.

## Deactivation Method
We found the best method is to simply change the SSID/Wireless network name to a string longer than 32 characters, which is the max length for SSID strings.  That has the effect of disabling the Wi-Fi and shows the message on the device screen.  We simply prefix the SSID with "Return Overdue Hotspot  ---------------------   ".

Pintrac support will tell you to make use of the time of day access feature, set to only allow access for 1 hour a day in the middle of the night, but we found that to be more complicated to setup, and we couldn't actually get it to work when we tested it, but didn't put a lot of effort into figuring out why.

## Install Instructions

### Callback script

This needs to be done on web servers that will be accessable to twilio making a request to "https://evergreen.domain/cgi-bin/pintrac-auth-sms/callback.pl".

Add the following section to /etc/apache2/sites-enabled/eg.conf on web servers.  Under the section titled "System config CGI scripts go here".  

```
Alias /cgi-bin/pintrac-auth-sms/ "/openils/var/cgi-bin/pintrac-auth-sms/"
<Directory "/openils/var/cgi-bin/pintrac-auth-sms">
        AddHandler cgi-script .pl
        AllowOverride None
        Options None
        Options ExecCGI
        <Files ~ "\.cfg(-example)?$">
          Order allow,deny
          Deny from all
        </Files>
</Directory>
```

Add the following section to /etc/apache2/eg_vhost.conf below the "staff-client offline scripts" section.

```
# ----------------------------------------------------------------------------------
# Module for processing pintrac authentication sms lives here
# ----------------------------------------------------------------------------------
<Directory "/openils/var/cgi-bin/pintrac-auth-sms">
    AddHandler cgi-script .pl
    AllowOverride None
    Options +ExecCGI
    Require all granted
</Directory>
```

Create /openils/var/cgi-bin/pintrac-auth-sms

Copy callback/callback.pl to /openils/var/cgi-bin/pntrac-auth-sms
Copy callback/twilio-auth.cfg-example to /openils/var/cgi-bin/pntrac-auth-sms/twilio-auth.cfg
Edit /openils/var/cgi-bin/pntrac-auth-sms/twilio-auth.cfg and set your values for the options.

```
mkdir /openils/var/cgi-bin/pintrac-auth-sms
cp callback/callback.pl /openils/var/cgi-bin/pintrac-auth-sms/
cp callback/twilio-auth.cfg-example /openils/var/cgi-bin/pintrac-auth-sms/twilio-auth.cfg
#Set twilio related variables
vim /openils/var/cgi-bin/pintrac-auth-sms/twilio-auth.cfg

sudo vim /etc/apache2/sites-enabled/eg.conf
sudo vim /etc/apache2/eg_vhost.conf
sudo systemctl reload apache2
#or
/etc/init.d/apache2 reload
```

### Processing script



##Workflow Breakdown
1. Run the process script every 10 minutes or so via cron.
2. Script checks for hotspots that need to be activated or deactivated.
3. Connect to pintrac API.  Use stored token or performs a new login.
4. If login is needed, request a new OTP from pintrac that goes to phone number associated with your twilio account.
5. Twilio sends sms message to callback script, which grabs the OTP and sends it back to the process script via Postgresq Notify.
6. Process script listens for event, and uses the OTP code to login.
7. Process script updates hotspot config as needed to activate or deactivate hotspots.

##Usage


# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
