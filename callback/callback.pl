#!/usr/bin/perl -T
use strict; use warnings;

use CGI ();

use XML::Simple;
use DBI;
use Config::File;

$| = 1; #no buffering output

my $authconfig = Config::File::read_config_file('./twilio-auth.cfg');

##Variables
my $ExpectedAccountSid = $authconfig->{'accountsid'}; #Twilio Account SID
my $ExpectedMessagingServiceSid = $authconfig->{'messagingservicesid'}; #Message
my @ExpectedFrom = map {$authconfig->{'phone'}{$_}} keys %{$authconfig->{'phone'}} ; #phone numbers

##Postgresql Notify Events
my $eventname = 'pintracauth';

##DB Setup
my $xmlconf = "/openils/conf/opensrf.xml";
my %dbconf = %{getDBconnects($xmlconf)};

my $dbh = DBI->connect("DBI:Pg:dbname=$dbconf{db};host=$dbconf{dbhost};port=$dbconf{port}",$dbconf{dbuser},$dbconf{dbpass},
{AutoCommit => 1})
  or die "Couldn't connect to database: " . DBI->errstr;

##CGI Setup
my $q = CGI->new;

my $MessagingServiceSid = $q->param('MessagingServiceSid') || '';
my $AccountSid = $q->param('AccountSid') || '';
my $From = $q->param('From');
my $To = $q->param('To');
my $Body = $q->param('Body');
#my $ = $q->param('');

#Sanity Check - does the account sid match what we expect?
if ($MessagingServiceSid ne $ExpectedMessagingServiceSid or $AccountSid ne $ExpectedAccountSid) {
	die "Wrong Service or Account ID, Exiting\n";
}

#Sender Check - does the sender match what we expect?
if ( !grep { $From eq $_ } @ExpectedFrom ){
	die "Wrong sender, Exiting\n";
} 

#Send response to Twilio
print $q->header();
print '<Response></Response>';

#Process body, sanitize code and send notify event
my $otp;
if ($Body =~ /Your? OTP is: (\d{6})/) {
	$otp = $1;
} else {
	die "No OTP found, exiting\n";
}

#Send notify with code as payload
$dbh->do("notify $eventname, '$otp';");

$dbh->disconnect;
exit(0);

sub getDBconnects
{
	my $openilsfile = $_[0];
	my $xml = new XML::Simple;
	my $data = $xml->XMLin($openilsfile);
	my %conf;
	$conf{"dbhost"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{host};
	$conf{"db"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{db};
	$conf{"dbuser"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{user};
	$conf{"dbpass"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{pw};
	$conf{"port"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{port};
	return \%conf;
}