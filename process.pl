#!/usr/bin/env perl
use strict; use warnings;

use XML::Simple;
use DBI;
use IO::Select;
use LWP::UserAgent ();
use JSON;
use Data::Dumper;
use Config::File;
use DateTime;
use Email::MIME;
use Email::Sender::Simple;
use Time::HiRes qw/ time /;
sub time_fp { int((sprintf"%d.%06d",Time::HiRes::gettimeofday)*1000) } #javascript millisecond timestamp

###Lock file so only one instance can run at a time.
use Fcntl qw(:flock);
open our $file, '<', $0 or die $!;
flock $file, LOCK_EX|LOCK_NB or die "Unable to lock file - already running $!";

#sleep(60); #delay for lockfile testing

my $DEBUG=0;

#$OUTPUT_AUTOFLUSH = 1; #no buffering output
$| = 1; #no buffering output
my $json = JSON->new->allow_nonref;

#Load config file
my $authconfig = Config::File::read_config_file('./auth.cfg');

##Store Token in file
my $tokenFile = './token.txt';
my $bearerToken = '';

##Database Connection Setup
my $xmlconf = "/openils/conf/opensrf.xml";
my %dbconf = %{getDBconnects($xmlconf)};
my $dbh = DBI->connect("DBI:Pg:dbname=$dbconf{db};host=$dbconf{dbhost};port=$dbconf{port}",$dbconf{dbuser},$dbconf{dbpass},
                       {AutoCommit => 1})
  or die "Couldn't connect to database: " . DBI->errstr;

##Postgresql Listen Events
my $eventname = 'pintracauth';

#Hotspot settings
my $wifiNamePrefix = 'Return Overdue Hotspot  ---------------------  '; #used to disable hotspots

#Pintrac Variables and settings.
my $UserAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36';

my $Username = $authconfig->{'username'};
my $Password = $authconfig->{'password'};

my $emailFrom = $authconfig->{'emailfrom'};#From address
my $additionalEmail = $authconfig->{'emailto'};#extra email recipients
my $emailCC = $authconfig->{'emailtocc'};#CC all emails to

my $pt_domain = 'mdmapi.pintracview.com';
my $pt_org_id = $authconfig->{'org_id'};

my $pt_request_otp  = "https://${pt_domain}/common/request/otp/${Username}"; #GET
my $pt_authenticate = "https://${pt_domain}/common/login"; #POST
my $pt_org_info     = "https://${pt_domain}/dashboard/orgs/${pt_org_id}"; #GET
my $pt_listdevices  = "https://${pt_domain}/dashboard/devices/org/${pt_org_id}/0"; #GET
my $pt_deviceconfig = "https://${pt_domain}/dashboard/device/"; #GET
my $pt_edit_device  = "https://${pt_domain}/dashboard/device/update"; #POST
my $pt_send_sms = "https://mdmapi.pintracview.com/dashboard/sendsms/IMEI#/+1/msisdn#"; #get


#### Current date and time strings
my $dt = DateTime->now(time_zone => "local"); 
my $fdate = $dt->ymd; 
my $ftime = $dt->hms;
my $dateString = "$fdate $ftime";

#### SQL Queries ###################################################
my $sqlGetHotspots = <<'EOT';
--- larl hotspots that need to be either deactivated or activated.

SELECT distinct x.* FROM
(
SELECT 
CASE when aou.shortname IS NULL AND ac.due_date IS NOT NULL THEN 'deactivate'
      when aou.shortname IS NOT NULL THEN 'activate'
      END pintrac_action

,aou.shortname checkin_lib,aou.email checkin_lib_email, aouco.shortname checkout_lib, ccs.name
,acp.id copyid, acp.status_changed_time
,acp.barcode, acn.label callnum
,SUBSTRING(acn.label FROM '% #"[0-9]{2,3}#"' FOR '#') hotspot_num
,ac.usr patronid
,acno.title notetitle, acno.value notevalue --, acno.id copynoteid
,ausp.note patron_note
--,ausp.id noteid
,ac.due_date, AGE(NOW(),ac.due_date) overdue_age, AGE(NOW(),acp.status_changed_time) status_changed_age
--,acpn.*

FROM asset.copy acp
JOIN asset.call_number acn ON acn.id=acp.call_number
JOIN config.copy_status ccs ON ccs.id=acp."status"
JOIN ACTION.circulation ac ON ac.target_copy=acp.id
                           AND ac.id = (SELECT id FROM ACTION.circulation
                                     WHERE target_copy=acp.id
                                         ORDER BY id desc
                                         LIMIT 1 )

LEFT OUTER JOIN actor.org_unit aou ON aou.id=ac.checkin_lib
LEFT OUTER JOIN actor.org_unit aouco ON aouco.id=ac.circ_lib
--LEFT OUTER JOIN actor.usr au ON au.id=ac.usr
LEFT OUTER JOIN asset.copy_note acno ON acno.owning_copy=acp.id 
                                     AND acno.title='DEACTIVATED'
LEFT OUTER JOIN actor.usr_standing_penalty ausp ON ausp.usr=ac.usr AND ausp.stop_date IS NULL
                                     AND ausp.note ~* 'Hotspot\s+\d{1,3}\s+Deactivated'
                                    
--LEFT OUTER JOIN asset.copy_note acpn ON acpn.owning_copy=acp.id
                                     

WHERE

acp.circ_modifier = 'Wi-Fi Hotspot'
AND NOT acp.deleted
AND acn.record=346058
AND acn.label~*'^Hotspot'
AND (
	   -- To Deactivate - Hotspots that are past their due date, not checked in and have no item notes.
     (ac.due_date <= NOW()-'3 days'::INTERVAL --Due dates are pushed to end of day , so 3 full days.
	  AND ac.checkin_time IS NULL 
	  AND acno.id IS null
	  )
     or
		 -- To activate - Hotspots that have been returned and checked in in the last 24 hours and were deactivated
     (ccs.name IN ('Available','Reshelving','In transit','On holds shelf','Damaged','Canceled Transit')
	  AND acp.status_changed_time >= NOW()-'24 hours'::interval
	  AND acno.id IS NOT null
		AND ac.checkin_time IS NOT null
 	  )
 	  
	)
	
-- Exclude items with copy notes that say "REMOVED PERMANENTLY" or "REPLACED"
AND NOT EXISTS (SELECT 1 FROM asset.copy_note acno1 WHERE acno1.owning_copy=acp.id 
                                     AND acno1.title IN ('REPLACED','REMOVED PERMANENTLY') )


Order by 1 --process activations first. 

LIMIT 20

) x

WHERE
-- Watch out for already deactivated hotspots that show up again
NOT (x.pintrac_action IS NOT DISTINCT FROM 'deactivate' AND x.notetitle IS NOT DISTINCT FROM 'DEACTIVATED')

ORDER BY 1
LIMIT 15
;

EOT

my $sqlSetCopyNote = <<'EOT';
insert into asset.copy_note
(owning_copy,creator,create_date,pub,title,value)
values
($1,1,now(),true,'DEACTIVATED',now()::date)
;
EOT

my $sqlClearCopyNote = <<'EOT';
delete from asset.copy_note
where
owning_copy=$1
and title='DEACTIVATED'
;
EOT

my $sqlSetPatronAlert = <<'EOT';
insert into actor.usr_standing_penalty
(org_unit, usr, standing_penalty, staff, set_date, note)
values
(1, $1, 25, 1, now(), 'Hotspot '||$2||' Deactivated')
;
EOT

my $sqlClearPatronAlert = <<'EOT';
update actor.usr_standing_penalty
set stop_date=now(),note=note||' - Reactivated on: '||now()::date
where
usr=$1
and stop_date is null
and note ~* '^Hotspot \d{2,3} Deactivated'
;
EOT


######  End SQL Queries  #######################################

####### Check to see if there are any hotspots that need processing #####
my $list = $dbh->selectall_arrayref($sqlGetHotspots) or die $dbh->errstr;
my $numhotspots = @$list;
print Dumper($list) if $DEBUG;

print "No hotspots matching criteria\n" if ! $numhotspots && $DEBUG;
exit if ! $numhotspots; #Stop if there is nothing to process

######  Prepare Queries ######
my $setNote_sth = $dbh->prepare($sqlSetCopyNote) or die $dbh->errstr;
my $clearNote_sth = $dbh->prepare($sqlClearCopyNote) or die $dbh->errstr;
my $setPen_sth = $dbh->prepare($sqlSetPatronAlert) or die $dbh->errstr;
my $clearPen_sth = $dbh->prepare($sqlClearPatronAlert) or die $dbh->errstr;


## If we have hotspots to process, proceed with pintrac login
checkLogin() or die "Not able to log in";


my $devicelist = getDeviceList();

print Dumper(\$devicelist) if $DEBUG;

#How many devices are there
print "Number of devices in pintrac: ".scalar @{ $devicelist }."\n";

#Map to a few hashs, so we can look up specifics.
my %hs_id_name = map { $_->{'_id'} => $_->{'color'} } @{ $devicelist };
my %hs_id_all  = map { $_->{'_id'} => $_ } @{ $devicelist };
#my %hs_name_id = map { (split(/ /,$_->{'color'}))[0] => $_->{'_id'} } @{ $devicelist };
##Handle leading zeros in hotspot numbers.
my %hs_name_id = map { my $c = $_->{'color'}; 
                       $c =~ s/^0?(\d{2,3}).*/$1/; 
                       $c => $_->{'_id'} 
					   } @{ $devicelist };


print Dumper(\%hs_name_id) if $DEBUG;

#Hotspots in the DB
#my $bibid = 346058;
#my $circmod = 'Wi-Fi Hotspot';

##loop through the results and apply changes.

my $delay=5;
foreach my $hotspot (@$list){

  #Delay to reduce load on pintrac and control accidental run aways
  print "Delay for $delay seconds\n" if $DEBUG;
  sleep($delay);
  $delay*=1.2; #increase delay by 20% each run

	#Map a few fields for ease of use.
	my ($mode,$hNum,$itemId,$patronId,$libEmail
  ,$itemBarcode,$checkinLib,$checkoutLib)=
	($hotspot->[0],$hotspot->[9],$hotspot->[5],$hotspot->[10],$hotspot->[2]
  ,$hotspot->[7],$hotspot->[1],$hotspot->[3]);
	#print $hs_name_id{$hNum};
	print "\n$mode - $hNum - $itemBarcode\n\n";
  #call with pintrac id and name
	changeConfigPintrac($hs_name_id{$hNum}, $hNum,$mode) or 
  die "Failed to update pintrac config, exiting\n";
  if ($mode eq 'deactivate'){
    setPatronAlert($patronId,$hNum);
    setCopyNote($itemId) or die "Problem setting copy note\n";
    logAction($hNum,$mode);
    sendEmailAlert($additionalEmail,$mode,$checkoutLib,$hNum,$itemBarcode);
  } elsif ($mode eq 'activate'){
    clearPatronAlert($patronId);
    clearCopyNote($itemId) or die "Problem clearing copy note\n";
    logAction($hNum,$mode);
    sendEmailAlert($additionalEmail.', '.$libEmail,$mode,$checkinLib,$hNum,$itemBarcode);
  }

}

$dbh->disconnect();
exit;

sub getPinTracOTP
{
	$dbh->do("LISTEN $eventname"); #start listening for events.

	my $fd = $dbh->{pg_socket}; #Grab filedescriptor for socket connection
	my $sel = IO::Select->new($fd);

	#Request OTP
	my $ua = LWP::UserAgent->new(timeout => 10);
	$ua->agent($UserAgent);
	my $response = $ua->get($pt_request_otp);

	if ($response->is_success) {
    print $response->decoded_content." - Request OTP\n";
	}
	else {
    die $response->status_line;
	}

	while (1) {
		print "waiting...";
		$sel->can_read(120); #wait for event notification
		my $notify = $dbh->func("pg_notifies");
		if ($notify) {
			my ($name, $pid, $payload) = @$notify;
			my $row = $dbh->selectrow_hashref("SELECT now()");
			print "$name from PID $pid with Payload $payload at $row->{now}\n";
			$dbh->do("UNLISTEN $eventname"); #Stop listening
			return $payload;
		} else {
			die "No OTP Received, exiting";
		}
	}
}

sub checkLogin
{
	#grab token
	my $token = getToken();
	if(!$token){
		#no stored token, so we will start a new login
		print "Token not found, so create new login\n";
		return 1 if loginPinTrac();
	} else {
		#We have a stored token, test to see if it still works.
		print "We have a token, check to see if it is legit\n";
    if( ! checkToken($token)){
			return 1 if loginPinTrac(); #Token didn't work, new login
		}
		return 1;
	}
  return 0;
}

### Check to see if the token is still valid
### Request device list for org with a limit of 1
sub checkToken
{
	my $token = $_[0];
  my $ua = LWP::UserAgent->new(timeout => 20);
	$ua->agent($UserAgent);
	my $res = $ua->get($pt_org_info,Authorization => 'Bearer '.$token);
	if($res->is_success) {
		print $res->decoded_content."\n" if $DEBUG;
	  $bearerToken = $token;
		return 1;
	} else {
    print "Token no longer valid\n";
		return 0;
	}
}

### Full Login to Pintrac
sub loginPinTrac
{
	my $otp = getPinTracOTP();

	my $loginContent = qq{{"email":"${Username}","password":"${Password}","otp":"${otp}"}};
	print $loginContent." is the loginContent\n" if $DEBUG;
	my $ua = LWP::UserAgent->new(timeout => 20);
	$ua->agent($UserAgent);
	my $res = $ua->post($pt_authenticate,Content => $loginContent,content_type => 'application/json' );
  
	if($res->is_success) {
		print $res->decoded_content."\n" if $DEBUG;
		#grab token
		my $jdata = $json->decode($res->content);
		print "\n".$jdata->{'token'}."\n";
		storeToken($jdata->{'token'}); #store token for future use.
		$bearerToken = $jdata->{'token'}; #set global
		return 1;
	} else {
		print "Request: ".$res->request->as_string();
		print "\nResponse: ".$res->as_string();
		die $res->status_line;
	}

}

#read token from file
sub getToken
{
  unless(-e $tokenFile) {
    #Create the file if it doesn't exist
    open(my $fc, '>', $tokenFile);
    close($fc);
  }
  open(my $fh, '<', $tokenFile) or die $!;
	my $token = <$fh>;
	close($fh);
	if(!$token){
		return 0;
	}
	else {
		return $token;
	}
}

#Store the token in file
sub storeToken
{
	my $token = $_[0];
	open(my $fh, '>', $tokenFile) or die $!;
	print $fh $token;
  close($fh);
}

sub getDeviceList
{
	my $ua = LWP::UserAgent->new(timeout => 20);
	$ua->agent($UserAgent);
	my $res = $ua->get($pt_listdevices,Authorization => 'Bearer '.$bearerToken);

	if($res->is_success) {
		my $jdata = $json->decode($res->content);
		return $jdata;
	} else {
		return 0;
	}

}
sub getDBconnects
{
	my $openilsfile = $_[0];
	my $xml = new XML::Simple;
	my $data = $xml->XMLin($openilsfile);
	my %conf;
	$conf{"dbhost"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{host};
	$conf{"db"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{db};
	$conf{"dbuser"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{user};
	$conf{"dbpass"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{pw};
	$conf{"port"}=$data->{default}->{apps}->{"open-ils.storage"}->{app_settings}->{databases}->{database}->{port};
	return \%conf;
}

sub changeConfigPintrac
{
	my ($id, $hotspotNum, $type) = @_;
  
	my $ua = LWP::UserAgent->new(timeout => 20);
	$ua->agent($UserAgent);
	
	#get up to date config
	my $res = $ua->get($pt_deviceconfig.$id,Authorization => 'Bearer '.$bearerToken);
  if($res->is_success) {
		print "Pintrac Hotspot Info: ".$res->decoded_content."\n" if $DEBUG;
	} else {
    print "Failed to fetch hotspot info: $id\n";
		return 0;
	}
  
	#change config
	my $jdata = $json->decode($res->content);
	print "Hotspot Data for id: $id \n".Dumper($jdata) if $DEBUG;

	## Add in leading zero for pintrac name - for sorting in pintrac 055 instead of 55
	my $pintracNum = $hotspotNum;
	$pintracNum =~ s/^(\d{2})$/0$1/;

  if ($type eq 'deactivate'){
    #change wireless name {'wireless'}{'wifiName'}
    $jdata->{'wireless'}{'wifiName'} = $wifiNamePrefix.'LARL Hotspot '.$hotspotNum;

    #change hotspot name {'color'}
	
    $jdata->{'color'} = $pintracNum.' - Deactivated on: '.$fdate;
    
    #change force update setting {'wireless'}{'saveConfigOnly'} = 0
    $jdata->{'wireless'}{'saveConfigOnly'} = 1;
  } elsif ($type eq 'activate'){
    #change wireless name {'wireless'}{'wifiName'}
    $jdata->{'wireless'}{'wifiName'} = 'LARL Hotspot '.$hotspotNum;

    #change hotspot name {'color'}
    $jdata->{'color'} = $pintracNum.' - Activated on: '.$fdate;
    
    #change force update setting {'wireless'}{'saveConfigOnly'} = 0
    $jdata->{'wireless'}{'saveConfigOnly'} = 1;
  }
	#change update timestamp {'updated'} , uses JS unix epoch format, with milliseconds.
	$jdata->{'updated'} = time_fp();

	#Clean out fields not normally sent back.
	delete $jdata->{'_id'};
	delete $jdata->{'actionRequired'};
	$jdata->{'configFwVersion'} = undef;
	delete $jdata->{'configMode'};
	delete $jdata->{'date_added'};
	delete $jdata->{'expires_on'};
	delete $jdata->{'isActive'};
	delete $jdata->{'latestIP'};
	delete $jdata->{'latestFwUpdateOn'};
	delete $jdata->{'latestFwVersion'};
	$jdata->{'notes'} = 'sample config for testing';
	delete $jdata->{'options'};
	delete $jdata->{'updatedAt'};
	delete $jdata->{'usage_in_byte'};
	delete $jdata->{'usage_in_gb'};
	delete $jdata->{'orgName'};
	$jdata->{'orgname'} = 'Lake Agassiz Library';
	delete $jdata->{'temp'};

	#config after updates
    print "Hotspot Data after update for id: $id \n".Dumper($jdata) if $DEBUG;
	#send updated config

	print $json->encode($jdata)."\n" if $DEBUG;

  $res = $ua->post($pt_edit_device,Content => $json->encode($jdata),
                   content_type => 'application/json',
                   Authorization => 'Bearer '.$bearerToken );

  if($res->is_success) {
		print "Pintrac Hotspot Config Updated : ".$res->decoded_content."\n";
	} else {
    print "Failed to Update Hotspot config: $id\n";
		return 0;
	}
}


sub setPatronAlert
{
  my ($usrId, $hotspotNum) = @_;
  
  my $rv = $setPen_sth->execute($usrId,$hotspotNum) 
  or die $setPen_sth->errstr;
  print "Set $rv standing penalty for userid: $usrId\n";
  return 1 if $rv ne '0E0';
}

sub clearPatronAlert
{
  my ($usrId) = @_;
  
  my $rv = $clearPen_sth->execute($usrId) 
  or die $clearPen_sth->errstr;
  print "Cleared $rv standing penalty for userid: $usrId\n";
  return 1 if $rv ne '0E0';
}


sub setCopyNote
{
  my ($copyId) = @_;
  
  my $rv = $setNote_sth->execute($copyId) 
  or die $setNote_sth->errstr;
  print "Set $rv copy note for itemid: $copyId\n";
  return 1 if $rv ne '0E0';
}

sub clearCopyNote
{
  my ($copyId) = @_;
  
  my $rv = $clearNote_sth->execute($copyId) 
  or die $clearNote_sth->errstr;
  print "Cleared $rv copy note for itemid: $copyId\n";
  return 1 if $rv ne '0E0';
}

sub sendEmailAlert {
  my ($libEmail,$mode,$libName,$hNum,$itemBarcode) = @_;
  
  my ($body,$subject);
  
  if($mode eq 'deactivate'){
    $subject = "Hotspot Deactivated: #$hNum - $itemBarcode - $libName";
    $body = <<"EOT";
Hotspot $hNum was deactivated on $dateString.
Hotspot Barcode: $itemBarcode

--
Automated Email from process.pl script on virt-egutil1:/home/opensrf/projects/pintrac-automation
EOT
  } elsif ($mode eq 'activate'){
    $subject = "Hotspot $hNum Activated: $itemBarcode - $libName";
    $body = <<"EOT";
Hello, 

Hotspot $hNum ($itemBarcode) was reactivated on $dateString.

To ensure hotspot $hNum is ready for the next customer, please turn on or restart 
the hotspot now. Around 2 minutes after turning on hotspot, check the screen and 
make sure it no longer says ( Return Overdue Hotspot ).

If the screen still says ( Return Overdue Hotspot ), plug in the hotspot and charge 
the battery, try again in 30 minutes.

Hotspots can not update configuration when the battery level is too low.  Charge to 
at least 50%, then restart the hotspots again and read the screen.

If the hotspot is still showing the ( Return Overdue Hotspot ) message after charging 
and restarting, try again one last time.  Sometimes it takes repeated attempts.

If the above attempts fail, mark the hotspot damaged in Evergreen, email a Help Ticket 
explaining the issue, and put Hotspot in delivery to Regional Office IT staff.

--
Automated Email from process.pl script on virt-egutil1:/home/opensrf/projects/pintrac-automation

EOT
  }

  my @parts = (
    Email::MIME->create(
      attributes => {
        content_type => 'text/plain',
        charset => 'utf-8',
        encoding => 'quoted-printable',
      },
      body_str => $body,
    ),
  );

  my $email = Email::MIME->create(
    header_str => [ From => $emailFrom,
                    To => $libEmail,
                    CC => $emailCC,
                    'Auto-Submitted' => 'auto-generated',
                    'Report-Type' => "Hotspot Status Change",
                    Location => "$libName",
                    System => "LARL",
                    Subject => $subject ],
    parts      => [ @parts ],
  );
  
  Email::Sender::Simple->try_to_send($email);

}

#Log actions to a log file
#Watch with Zabbix and alert if there are no
#file modifications in a certain number of days
sub logAction {
  my ($hNum,$mode) = @_;
  open(my $fh, '>>', './process.log') or die $!;
  print $fh "$dateString - Hotspot $hNum - $mode\n";
  close($fh);
}
